import React, { PureComponent } from "react";
import Autocomplete from "react-autocomplete";
import throttle from "lodash/throttle";
import Helmet from "react-helmet";
import './routes.css';

import DistanceRoutes from "./distance-routes.component";
import "./distance-routes.component.css";

/*eslint-disable no-undef*/
export class Routes extends PureComponent {

    constructor ( props ) {
        super( props );
        this.state = {};

        this.searchOrigin = throttle( this.searchOrigin.bind( this ), 200, {
            leading: false
        });
        this.searchDestination = throttle( this.searchDestination.bind( this ), 200, {
            leading: false
        });
        this.onChangeOrigin = this.onChangeOrigin.bind( this );
        this.onSelectOrigin = this.onSelectOrigin.bind( this );
        this.onChangeDestination = this.onChangeDestination.bind( this );
        this.onSelectDestination = this.onSelectDestination.bind( this );
    }

    searchOrigin ( value ) {
        if ( !value ) {
            this.setState({ sourceOrigins: [] });
            return;
        }

        this.autocompleter = this.autocompleter || new google.maps.places.AutocompleteService();
        this.autocompleter.getPlacePredictions({
            input: value,
            types: [ "establishment", "geocode" ]
        }, ( results, status ) => {
            if ( status !== "OK" && status !== "ZERO_RESULTS" ) {
                return;
            }

            const sourceOrigins = results.map( result => ({
                placeId: result.place_id,
                description: result.description
            }));

            this.setState({ sourceOrigins });
        });
    }

    onChangeOrigin ( value ) {
        this.setState({ origin: value });
        this.searchOrigin( value );
    }

    onSelectOrigin ( value ) {
        const origin = this.state.sourceOrigins.find( origin => origin.placeId === value ).description;
        this.setState({ selectedOrigin: value, origin });
    }

    searchDestination ( value ) {
        if ( !value ) {
            this.setState({ sourceDestinations: [] });
            return;
        }

        this.autocompleter = this.autocompleter || new google.maps.places.AutocompleteService();
        this.autocompleter.getPlacePredictions({
            input: value,
            types: [ "establishment", "geocode" ]
        }, ( results, status ) => {
            if ( status !== "OK" && status !== "ZERO_RESULTS" ) {
                return;
            }

            const sourceDestinations = results.map( result => ({
                placeId: result.place_id,
                description: result.description
            }));

            this.setState({ sourceDestinations });
        });
    }

    onChangeDestination ( value ) {
        this.setState({ destination: value });
        this.searchDestination( value );
    }

    onSelectDestination ( value ) {
        const destination = this.state.sourceDestinations.find( origin => origin.placeId === value ).description;
        this.setState({ selectedDestination: value, destination });
    }

    render () {
        const { sourceOrigins = [], sourceDestinations = [], origin, destination } = this.state;

        return (
            <div id="content">
                <Helmet
                    script={[{ src: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBFEHVRN3JDGKQcCM4_dlika7SkPdrj-0k&libraries=places" }]}
                />

                <label>From:</label>

                <Autocomplete
                    getItemValue={ item => item.placeId }
                    items={ sourceOrigins }
                    wrapperStyle={{ "zIndex" : "99999", position: "relative" }}
                    renderItem={ ( item, isHighlighted ) =>
                        <div style={{ background: isHighlighted ? "lightgray" : "white", cursor: "pointer" }} >
                            { item.description }
                        </div>
                    }
                    value={ origin }
                    onChange={ event => this.onChangeOrigin( event.target.value ) }
                    onSelect={ this.onSelectOrigin }
                />

                <label>To:</label>    

                <Autocomplete
                    getItemValue={ item => item.placeId }
                    items={ sourceDestinations }
                    wrapperStyle={{ "zIndex" : "99999", position: "relative" }}
                    renderItem={ ( item, isHighlighted ) =>
                        <div style={{ background: isHighlighted ? "lightgray" : "white", cursor: "pointer" }} >
                            { item.description }
                        </div>
                    }
                    value={ destination }
                    onChange={ event => this.onChangeDestination( event.target.value ) }
                    onSelect={ this.onSelectDestination }
                />
                <br/>

                <DistanceRoutes origin={ this.state.selectedOrigin } destination={ this.state.selectedDestination } />
            </div>
        );
    }
}

export default Routes;