import React, { PureComponent } from "react";

import "./distance-routes.component.css";

/*eslint-disable no-undef*/
export class DistanceRoutes extends PureComponent {

    constructor ( props ) {
        super( props );
        this.initMap = this.initMap.bind( this );
        this.state = {};
    }

    componentDidMount () {
        this.initMap();
    }

    componentWillReceiveProps ( nextProps ) {
        const { origin, destination } = nextProps;
        if ( !origin || !destination ) {
            return;
        }

        const request = {
            origin: { placeId: origin },
            destination: { placeId: destination },
            travelMode: "DRIVING"
        };
        this.directionsService = this.directionsService || new google.maps.DirectionsService();
        this.directionsService.route( request, ( result, status ) => {
            const distances = [];
            if ( status == "OK" ) {
                this.directionsDisplay.setDirections( result );
                const { routes } = result;
                routes.forEach( route => {
                    route.legs.forEach( leg => {
                        distances.push({ text: leg.distance.text, duration: leg.duration.text });
                    });
                });
            }
            this.setState({ distances });
        });
    }

    initMap () {
        try {
            this.directionsDisplay = new google.maps.DirectionsRenderer();
            var chicago = new google.maps.LatLng( 41.850033, -87.6500523 );
            var mapOptions = {
                zoom:7,
                center: chicago
            };
            const map = new google.maps.Map( this.map, mapOptions );
            this.directionsDisplay.setMap( map );
        } catch ( e ) {
            setTimeout( this.initMap, 100 );
        }
    }

    render () {
        const { distances = [] } = this.state;

        return (
            <div>
                <div ref={ element => { this.map = element; } } className="map" />
                {
                    distances.map( ( distance, index ) => (
                        <div key={ index } >
                            { "Route: " }
                            <span>{ distance.text }</span>
                            { " - " }
                            <span>{ distance.duration }</span>
                        </div>
                    ))
                }
            </div>
        )
    }
}

export default DistanceRoutes;